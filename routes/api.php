<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('userdata','User\UserController@getuser');
Route::post('userdata','User\UserController@usersave');
Route::post('login','User\UserController@login');
Route::get('userdata/{api_token}','User\UserController@getuserid');
Route::post('updateuserdata/{api_token}','User\UserController@updateuser');
Route::get('deleteuserdata/{api_token}','User\UserController@deleteuser');