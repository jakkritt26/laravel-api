<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    
    public function getuser(){
        return response()->json(UserModel::get(),200);
    }

    public function getuserid($api_token){

        $user = UserModel::where('api_token', $api_token)->get();
        if(count($user)<=0){
            return response()->json(['messaeg'=>'User not found']);
        }else{
            return response()->json($user);
        }
        
    }

    public function updateuser($api_token,Request $request){
        $user_check = UserModel::where('api_token', $api_token)->get();
        //return response()->json($user_check,200);
        $return_data = array();
       // $requestData = $request->all();
        if(count($user_check)>0){
            
            $update = UserModel::where('api_token', $api_token)->update($request->all());
            $return_data['status'] = 200;
            $return_data['message'] = "updated";
        }else{
            $return_data['status'] = 400;
            $return_data['message'] = 'User Not Found Check your api_token';
        }
        return response()->json($return_data);
    }


    public function deleteuser($api_token){

        $user_check = UserModel::where('api_token', $api_token)->get();
        $return_data = array();       
        if(count($user_check)>0){
            $update = UserModel::where('api_token', $api_token)->delete();
            $return_data['status'] = 200;
            $return_data['message'] = "updated";
        }else{
            $return_data['status'] = 400;
            $return_data['message'] = 'User Not Found Check your api_token';
           
        }
        return response()->json($return_data);
    }

    
    public function login(Request $request){
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];
       
       $requestData = $request->all();
        $password  = "";
        $user = "";
        if(isset($requestData['password'])) $password = md5($requestData['password']);    
        if(isset($requestData['email'])) $user = $requestData['email'];
        //echo $password;
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json($validator->errors());
        }
        $user_check = UserModel::where('email',$user)
        ->get();

       
        $return_data = array();       
        if(count($user_check)>0){
            $user_check_passs = UserModel::where('email',$user)
            ->where('password',$password)
            ->get();
            if(count($user_check_passs)>0){
            return response()->json($user_check_passs);
            }else{
                return response()->json(['message'=>'Password incorrect','status'=>'200']);
            }
        }else{
            $return_data['status'] = 400;
            $return_data['message'] = 'User Not Found';
            return response()->json($return_data);
        }
    }

    public function usersave(Request $request)
    {
       //dd($request);
       $requestData = $request->all();
       $request->merge([
        'password' => md5($requestData['password']),
        'api_token'=>hash('sha256', Str::random(60))
        ]);
      
       $user = UserModel::create($request->all());
       $return_data = array();
       
      if($user){
        $return_data['status'] = 200;
       
      }else{
        $return_data['status'] = 400;
      }
      return response()->json($return_data,200);
    }
}


