<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table ="users";

    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'tel',
        'level',
        'remember_token',
        'created_at',
        'update_at',
        'api_token'
    ];
    //
}
